<?php

namespace Drupal\textwatcher\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "textwatcher" plugin.
 *
 * @CKEditorPlugin(
 *   id = "textwatcher",
 *   label = @Translation("Text Watcher"),
 *   module = "textwatcher"
 * )
 */
class TextWatcher extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return 'libraries/textwatcher/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [];
  }

}
