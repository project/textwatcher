INTRODUCTION
============
Adds in the Textwatcher plugin for CKEditor.

This is required by plugins that need textwatcher plugin to work,
like Autocomplete.

REQUIREMENTS
============
This module requires the core CKEDITOR module.

CONFIGURATION
============
Other plugins can leverage textwatcher by adding this in their plugin
definition:

```
class YourButtonPlugin extends CKEditorPluginBase 
implements CKEditorPluginConfigurableInterface {
  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return ['textwatcher'];
  }

  // more code...
```

INSTALLATION
============

This module requires the core CKEditor module.

1.Download the library from https://ckeditor.com/cke4/addon/textwatcher.

2.Place the library in the root libraries folder (/libraries).

3.Enable Textwatcher module.

LIBRARY INSTALLATION (COMPOSER)
-------------------------------
1.Copy the following into your project's composer.json file.

```
"repositories": [
  "ckeditor-plugin/textwatcher": {
    "type": "package",
    "package": {
      "name": "ckeditor-plugin/textwatcher",
      "version": "4.11.4",
      "type": "drupal-library",
      "dist": {
        "url": "https://download.ckeditor.com/textwatcher/releases/textwatcher_4.11.4.zip",
        "type": "zip"
      }
    }
  }
]
```
2.Ensure you have following mapping inside your composer.json.
```
"extra": {
  "installer-paths": {
    "libraries/{$name}": ["type:drupal-library"]
  }
}
```
3.Run following command to download required library.
```
composer require ckeditor-plugin/textwatcher
```
4.Enable the Textwatcher module.
